OBJS = Rsa.o Client.o Server.o
EXES = rsaDriver client server

all: ${EXES}

rsaDriver: RsaDriver.o Rsa.o
	g++ -g -o rsaDriver RsaDriver.o Rsa.o
client: Client.o Rsa.o
	g++ -g -o client Client.o Rsa.o
server: Server.o Rsa.o
	g++ -g -lpthread -o server Server.o Rsa.o
Rsa.o: Rsa.cpp Rsa.h
	g++ -g -c -o Rsa.o Rsa.cpp
RsaDriver.o: RsaDriver.cpp
	g++ -g -c -o RsaDriver.o RsaDriver.cpp
Client.o: Client.cpp
	g++ -g -c -o Client.o Client.cpp
Server.o: Server.cpp
	g++ -g -c -o Server.o Server.cpp

.PHONY: all clean run

run:
	./rsaDriver

clean:
	rm -rfv ${EXES} ${OBJS}