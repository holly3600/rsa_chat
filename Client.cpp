#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <unistd.h>

#include "Rsa.h"

#define SERVER_PORT 11011
#define MAX_PENDING 5
#define MAX_LINE 256

int split(char reg,char* buf,int size,char* dest0,char* dest1){
	char* p = strchr(buf,reg);
	if (p != NULL){
		int index = p - buf;
		strncpy(dest0, buf, index);
		dest0[index] = 0;
		strncpy(dest1, buf+index+1, size - index -1);
		dest1[size - index -1] = 0;
		return 1;
	}else{
		return -1;
	}
}

int main(int argc, char * argv[]){
	struct hostent *hp;
	struct sockaddr_in sin;
	char *host;
	char buf[MAX_LINE],temp[MAX_LINE],dest0[MAX_LINE], dest1[MAX_LINE];
	int s;
	int recv_len;
	fd_set master, read_fds;
	int fdmax;

	if (argc==2) {
		host = argv[1];
	}
	else {
		fprintf(stderr, "usage: client host\n");
		exit(1);
	}

	/* translate host name into peer’s IP address */
	hp = gethostbyname(host);
	if (!hp) {
		fprintf(stderr, "client: unknown host: %s\n", host);
		exit(1);
	}
	/* build address data structure */
	bzero((char *)&sin, sizeof(sin));
	sin.sin_family = AF_INET;
	bcopy(hp->h_addr, (char *)&sin.sin_addr, hp->h_length);
	sin.sin_port = htons(SERVER_PORT);
	/* active open */
	if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("client: socket");
		exit(1);
	}
	if (connect(s, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
		perror("client: connect");
		close(s);
		exit(1);
	}

	//exchange public keys
	long key_pair[2][2]; //key_pair[0] for private
	long server_pub_key[2];
	long prime1;
	long prime2;
	long encrypted;
	long decrypted;
	Rsa::generateRandomKeyPair(key_pair, &prime1, &prime2);

	//recv server public key
	bzero(buf,sizeof(buf));
	recv_len = recv(s, buf, sizeof(buf), 0);
	buf[MAX_LINE-1] = '\0';
	if (!split(' ',buf,strlen(buf),dest0,dest1)){
		fprintf(stderr, "split error\n");
		return 1;
	}
	server_pub_key[0] = atol(dest0);
	server_pub_key[1] = atol(dest1);

	//send client public key
	bzero(buf,sizeof(buf));
	bzero(temp,sizeof(temp));
	sprintf(temp,"%ld ",key_pair[1][0]);
	strcpy(buf, temp);
	bzero(temp,sizeof(temp));
	sprintf(temp,"%ld",key_pair[1][1]);
	strcat(buf,temp);
	// printf("buf = %s\n",buf);
	send(s, buf, strlen(buf), 0);

	printf("keypair key: c = %ld; e = %ld; d = %ld;\n",key_pair[1][0], key_pair[1][1],key_pair[0][1]);
	printf("server public key: c = %ld; e = %ld;\n",server_pub_key[0], server_pub_key[1]);
	printf("sizeof(long) = %d\n",sizeof(long));

	FD_ZERO(&master);    // clear the master and temp sets
	FD_ZERO(&read_fds);
	FD_SET(s,&master);
	FD_SET(STDIN_FILENO,&master);
	fdmax = s > STDIN_FILENO ? s : STDIN_FILENO;

	while(1) {
		read_fds = master;
		if (select(fdmax + 1,&read_fds,NULL,NULL,NULL) == -1){
			perror("select");
			exit(1);
		}
		for (int i = 0;i <= fdmax;i++){
			if (FD_ISSET(i,&read_fds)){
				if (i == STDIN_FILENO){ //handle input
					bzero(buf,sizeof(buf));
					bzero(temp,sizeof(temp));
					fgets(temp,sizeof(temp),stdin);
					// scanf("%s",temp);
					recv_len = strlen(temp);
					for (int i = 0; i < recv_len; ++i){
						decrypted = temp[i];
						encrypted = Rsa::endecrypt(decrypted, server_pub_key[1], server_pub_key[0]);
						// printf("%c\n", decrypted);
						// printf("%ld\n", decrypted);
						// printf("%ld\n", encrypted);
						bzero(dest0,sizeof(dest0));
						sprintf(dest0,"%ld ",encrypted);
						strcat(buf, dest0);
					}
					buf[strlen(buf) - 1] = '\0';
					send(s, buf, strlen(buf), 0);
					printf("sent out encrypted:\n%s\n",buf);
				}else if (i == s){ //handle socket data
					bzero(buf,sizeof(buf));
					recv_len = recv(s,buf,sizeof(buf),0);
					printf("recv_len = %d\n",recv_len);
					if (recv_len == 0){
						return 1;
					}
					printf("Server says:\n");
					printf("encrypted:\n");
					printf("%s\n",buf);
					printf("decrypted:\n");
					while(split(' ',buf,strlen(buf),dest0,dest1) != -1){
						// printf("dest0 = %s\n",dest0);
						encrypted = atol(dest0);
						decrypted = Rsa::endecrypt(encrypted, key_pair[0][1], key_pair[0][0]);
						printf("%c",decrypted);
						// printf("long = %ld\n",decrypted);
						strcpy(buf, dest1);
					}
					encrypted = atol(buf);
					decrypted = Rsa::endecrypt(encrypted, key_pair[0][1], key_pair[0][0]);
					printf("%c\n",decrypted);
					// printf("long = %ld\n",decrypted);
				}else{
					fprintf(stderr, "Something is wrong with select\n");
				}
			}//end if FD_ISSET
		}//end for
	}//end while
}
