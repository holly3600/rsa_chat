#include "Rsa.h"
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <cstdio>

long 
Rsa::coprime(long x){
	srand(time(NULL));
	long ran;
	while(1){
		ran = rand() % x;
		if (GCD(x,ran) == 1){
			break;
		}
	}
	return ran;
}

long 
Rsa::endecrypt(long msg_or_cipherm, long key, long c){
	long crypt = modulo(msg_or_cipherm, key, c);
	return crypt;
}

long 
Rsa::GCD(long a, long b){
	long temp = b;
	while(b != 0){
		temp = b;
		b = a % b;
		a = temp;
	}
	return a;
}

long 
Rsa::mod_inverse(long base, long m){
	long d = modulo(base, totient(m)-1, m);
	return d;
}

long 
Rsa::modulo(long a, long b, long c){
	long mod = 1;
	for (int i = 0; i < b; ++i){
		mod = mod * a;
		mod = mod % c;
	}
	return mod;
}

long 
Rsa::totient(long n){
	long phi = n;
	for (int i = 2; i*i <= n; i++){
		if (n % i == 0){
			phi = phi - phi/i;
		}
		while(n % i == 0){
			n /= i;
		}
	}
	if (n > 1){
		phi = phi - phi/n;
	}
	return phi;
}

void 
Rsa::generateRandomKeyPair(long keypair[2][2], long* prime1, long* prime2){
	srand(time(NULL));
	int ith = rand() % MAX_NUMBER_PRIME;
	//ith = 23;
	*prime1 = getIthPrime(ith);
	ith = rand() % MAX_NUMBER_PRIME;
	//ith = 57;
	*prime2 = getIthPrime(ith);
	while(prime1 == prime2){
		ith = rand() % MAX_NUMBER_PRIME;
		*prime2 = getIthPrime(ith);
	}
	generateKeyPair(keypair, *prime1, *prime2);
	// generateKeyPair(keypair, 499, 433);
}

void 
Rsa::generateKeyPair(long keypair[2][2], long a, long b){
	if (a == b){
		generateRandomKeyPair(keypair, &a, &b);
		return;
	}
	long c = a * b;
	long m = (a-1)*(b-1);
	long e = coprime(m);
	long d = mod_inverse(e,m);
	keypair[0][0] = c;
	keypair[1][0] = c;
	keypair[0][1] = d;
	keypair[1][1] = e;
	// long primes[2];
	// crack(c,primes);
	// printf("prime1 = %ld\n", a);
	// printf("prime2 = %ld\n", b);
	// printf("prime[0] = %ld\n", primes[0]);
	// printf("prime[1] = %ld\n", primes[1]);
}

long 
Rsa::getIthPrime(int i){
	long j = 1;
	int count = 0;
	while (count <= i){
		++j;
		if (isPrime(j)){
			++count;
		}
	}
	return j;
}

bool
Rsa::isPrime(long number){
	for (int i = 2; i <= sqrt(number); ++i){
		if (number % i == 0){
			return false;
		}
	}
	return true;
}

bool
Rsa::crack(long c, long primes[2]){
	for (long a = 2; a <= sqrt(c); ++a){
		if (c % a == 0){
			primes[0] = a;
			primes[1] = c/a;
			return true;
		}
	}
	return false;
}

// int main(){
// 	long keypair[2][2];
// 	long prime1 = Rsa::getIthPrime(13);
// 	long prime2 = Rsa::getIthPrime(17);
// 	Rsa::generateKeyPair(keypair,prime1,prime2);
// 	std::cout<<"c = "<<keypair[0][0]<<"; e = "<<keypair[1][1]<<"; d = "<<keypair[0][1]<<std::endl;
// }
