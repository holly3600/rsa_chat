#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>

#include "Rsa.h"

void genKey();
void encrypt();
void decrypt();
void rsaCraker();


int main(){
	printf("Welcome to Rsa Driver, please select:\n");
	printf("1.Generate Key.\n");
	printf("2.Encrypt.\n");
	printf("3.Decrypt.\n");
	printf("4.RSA craker.\n");
	int choice = 0;
	scanf("%d",&choice);
	switch(choice){
		case 1:
			genKey();
			break;
		case 2:
			encrypt();
			break;
		case 3:
			decrypt();
			break;
		case 4:
			rsaCraker();
			break;
		default:
			printf("Please entry an valid number!\n");
			break;
	}
}

void genKey(){
	printf("Enter the nth prime and the mth prime to compute: use space as delimeter\n");
	int ith,jth;
	long keypair[2][2];
	scanf("%d %d",&ith,&jth);
	int prime1 = Rsa::getIthPrime(ith);
	int prime2 = Rsa::getIthPrime(jth);
	Rsa::generateKeyPair(keypair, prime1, prime2);
	printf("%dth prime = %d, %dth prime = %d\n",ith,prime1,jth,prime2);
	printf("c = %d, m = %d, e = %d, d = %d\n",keypair[0][0],(prime1-1)*(prime2-1),keypair[1][1],keypair[0][1]);
	printf("Public Key = (%d, %d), Private Key = (%d, %d)\n", keypair[1][1],keypair[1][0],keypair[0][1],keypair[0][0]);
}

void encrypt(){
	printf("Please enter the public key e c: use space as delimeter(first e, then c)\n");
	long e,c;
	scanf("%ld %ld",&e,&c);
	char temp[1024],buf[1024],dest0[1024];
	int len;
	long encrypted;
	long decrypted;
	while(1){
		printf("Please enter a sentence to encrypt:\n");
		fgets(temp,sizeof(temp),stdin);
		// scanf("%s",temp);
		len = strlen(temp);
		for (int i = 0; i < len; ++i){
			decrypted = temp[i];
			encrypted = Rsa::endecrypt(decrypted, e, c);
			// printf("%c\n", decrypted);
			// printf("%ld\n", decrypted);
			// printf("%ld\n", encrypted);
			bzero(dest0,sizeof(dest0));
			sprintf(dest0,"%ld ",encrypted);
			strcat(buf, dest0);
		}
		buf[strlen(buf) - 1] = '\0';
		printf("encrypted:\n%s\n",buf);
	}
}
void decrypt(){
	printf("Please enter the private key d c: use space as delimeter(first d, then c)\n");
	long d,c;
	scanf("%ld %ld",&d,&c);
	char temp[1024],buf[1024];
	int len;
	long encrypted;
	long decrypted;
	while(1){
		printf("Enter next char cipher value as a long:\n");
		scanf("%s",temp);
		encrypted = atol(temp);
		decrypted = Rsa::endecrypt(encrypted, d, c);
		printf("%c %ld\n",decrypted, decrypted);
	}
}

void rsaCraker(){
	printf("Enter the public key value: use space as delimeter(first e then c)\n");
	long e,c;
	scanf("%ld %ld",&e,&c);
	long primes[2];
	Rsa::crack(c,primes);
	long m = (primes[0]-1)*(primes[1]-1);
	long d = Rsa::mod_inverse(e,m);
	printf("a was %ld, b was %ld\n", primes[0], primes[1]);
	printf("The totient is %ld\n", Rsa::totient(m));
	printf("D was found to be %ld\n", d);

	long encrypted;
	long decrypted;
	while(1){
		printf("Enter a letter to decrypt.\n");
		scanf("%ld",&encrypted);
		decrypted = Rsa::endecrypt(encrypted, d, c);
		printf("This long decrypt to %ld\n",decrypted);
		printf("This letter is %c\n",decrypted);
	}
}
