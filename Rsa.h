
#define MAX_NUMBER_PRIME 100

class Rsa{
public:
	static long coprime(long x);
	static long endecrypt(long msg_or_cipherm, long key, long c);
	static long GCD(long a, long b);
	static long mod_inverse(long base, long m);
	static long modulo(long a, long b, long c);
	static long totient(long n);
	static void generateRandomKeyPair(long keypair[2][2], long* prime1, long* prime2);
	static void generateKeyPair(long keypair[2][2], long a, long b);
	static long getIthPrime(int i);
	static bool isPrime(long number);
	static bool crack(long c, long primes[2]);
};
